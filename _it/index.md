---
title: Copertina
layout: default
homepage: true
no_header: true
root: ..
idx: 0.1
---

<header>
    <h1>Minetest: Libro del Moddaggio</h1>

    <span>di <a href="https://rubenwardy.com" rel="author">rubenwardy</a></span>
    <span>con modifiche di <a href="http://rc.minetest.tv/">Shara</a></span>
    <span>traduzione italiana di <a href="https://liberapay.com/Zughy/">Zughy</a></span>
</header>

## Introduzione

Il moddaggio su Minetest è supportato grazie a script in Lua.
Questo libro mira a insegnarti come creare le tue mod, iniziando dalle basi.
Ogni capitolo si concentra su un punto specifico dell'API, portandoti in breve tempo
a fare le tue mod.

Oltre che [leggere questo libro online](https://rubenwardy.com/minetest_modding_book),
puoi anche [scaricarlo in HTML](https://github.com/rubenwardy/minetest_modding_book/releases).

### Riscontri e Contributi

Hai notato un errore o vuoi darmi il tuo parere? Assicurati di farmelo presente.

* Apri una [Segnalazione su GitLab](https://gitlab.com/rubenwardy/minetest_modding_book/-/issues).
* Rispondi alla [Discussione sul Forum](https://forum.minetest.net/viewtopic.php?f=14&t=10729).
* [Contattami (in inglese)](https://rubenwardy.com/contact/).
* Voglia di contribuire?
  [Leggi il README](https://gitlab.com/rubenwardy/minetest_modding_book/-/blob/master/README.md).
